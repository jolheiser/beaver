package main

import (
	"fmt"
	"os"

	"go.jolheiser.com/beaver"
	"go.jolheiser.com/beaver/color"
)

func main() {

	// Formatting
	fmt.Println(color.New(color.Reset).Format("Reset"))
	fmt.Println(color.New(color.Bold).Format("Bold"))
	fmt.Println(color.New(color.Faint).Format("Faint"))
	fmt.Println(color.New(color.Italic).Format("Italic"))
	fmt.Println(color.New(color.Underline).Format("Underline"))
	fmt.Println(color.New(color.BlinkSlow).Format("BlinkSlow"))
	fmt.Println(color.New(color.BlinkRapid).Format("BlinkRapid"))
	fmt.Println(color.New(color.ReverseVideo).Format("ReverseVideo"))
	fmt.Println(color.New(color.Concealed).Format("Concealed"))
	fmt.Println(color.New(color.CrossedOut).Format("CrossedOut"))

	// Foreground
	fmt.Println(color.New(color.FgBlack).Format("FgBlack"))
	fmt.Println(color.New(color.FgRed).Format("FgRed"))
	fmt.Println(color.New(color.FgGreen).Format("FgGreen"))
	fmt.Println(color.New(color.FgYellow).Format("FgYellow"))
	fmt.Println(color.New(color.FgBlue).Format("FgBlue"))
	fmt.Println(color.New(color.FgMagenta).Format("FgMagenta"))
	fmt.Println(color.New(color.FgCyan).Format("FgCyan"))
	fmt.Println(color.New(color.FgWhite).Format("FgWhite"))
	// Hi
	fmt.Println(color.New(color.FgHiBlack).Format("FgHiBlack"))
	fmt.Println(color.New(color.FgHiRed).Format("FgHiRed"))
	fmt.Println(color.New(color.FgHiGreen).Format("FgHiGreen"))
	fmt.Println(color.New(color.FgHiYellow).Format("FgHiYellow"))
	fmt.Println(color.New(color.FgHiBlue).Format("FgHiBlue"))
	fmt.Println(color.New(color.FgHiMagenta).Format("FgHiMagenta"))
	fmt.Println(color.New(color.FgHiCyan).Format("FgHiCyan"))
	fmt.Println(color.New(color.FgHiWhite).Format("FgHiWhite"))

	// Background
	fmt.Println(color.New(color.BgBlack).Format("BgBlack"))
	fmt.Println(color.New(color.BgRed).Format("BgRed"))
	fmt.Println(color.New(color.BgGreen).Format("BgGreen"))
	fmt.Println(color.New(color.BgYellow).Format("BgYellow"))
	fmt.Println(color.New(color.BgBlue).Format("BgBlue"))
	fmt.Println(color.New(color.BgMagenta).Format("BgMagenta"))
	fmt.Println(color.New(color.BgCyan).Format("BgCyan"))
	fmt.Println(color.New(color.BgWhite).Format("BgWhite"))
	// Hi
	fmt.Println(color.New(color.BgHiBlack).Format("BgHiBlack"))
	fmt.Println(color.New(color.BgHiRed).Format("BgHiRed"))
	fmt.Println(color.New(color.BgHiGreen).Format("BgHiGreen"))
	fmt.Println(color.New(color.BgHiYellow).Format("BgHiYellow"))
	fmt.Println(color.New(color.BgHiBlue).Format("BgHiBlue"))
	fmt.Println(color.New(color.BgHiMagenta).Format("BgHiMagenta"))
	fmt.Println(color.New(color.BgHiCyan).Format("BgHiCyan"))
	fmt.Println(color.New(color.BgHiWhite).Format("BgHiWhite"))

	// Presets
	beaver.Console.Level = beaver.TRACE
	beaver.Console.Format.LevelPrefix = true
	beaver.Console.Format.LevelColor = true
	beaver.Log(beaver.TRACE, "Trace")
	beaver.Log(beaver.DEBUG, "Debug")
	beaver.Log(beaver.INFO, "Info")
	beaver.Log(beaver.WARN, "Warn")
	beaver.Log(beaver.ERROR, "Error")
	beaver.Log(beaver.FATAL, "Fatal")

	// Time Prefix
	b := beaver.New(os.Stdout, beaver.INFO, beaver.FormatOptions{
		TimePrefix:   true,
	})
	b.Info("\t\tTime")

	// Stack Prefix
	b.Format = beaver.FormatOptions{
		StackPrefix:  true,
		StackLimit:   25,
	}
	b.Info("Stack")

	// All options, non-color
	b.Format = beaver.FormatOptions{
		TimePrefix:   true,
		StackPrefix:  true,
		LevelPrefix:  true,
	}
	b.Info("Full options with no color")

	// All options, Gitea colors
	b.Format = beaver.FormatOptions{
		TimePrefix:   true,
		StackPrefix:  true,
		StackLimit:   20,
		LevelPrefix:  true,
		LevelColor:   true,
		MessageColor: false,
	}
	color.Time = color.New(color.FgCyan)
	color.Stack = color.New(color.FgGreen)
	b.Info("Full options emulating Gitea")

	// Extended example
	ex := color.NewExtended(color.Gold1, color.DarkGreen)
	fmt.Println(ex.Format("Gold on Dark Green"))

	// True color example
	birb := color.NewTrue(color.MustParseHex("#007D96"), &color.RGB{})
	fmt.Println(birb.Format("Birb blue on black"))
}
