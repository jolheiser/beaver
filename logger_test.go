package beaver

import (
	"bytes"
	"fmt"
	"os"
	"strings"
	"testing"
)

var testBuffer = &bytes.Buffer{}

const MESSAGE = "Test"

func TestMain(m *testing.M) {
	os.Exit(m.Run())
}

func TestLogger(t *testing.T) {
	log := New(testBuffer, TRACE, FormatOptions{
		MessageColor: true,
	})
	tt := []struct {
		Name  string
		Level Level
	}{
		{Name: "Trace Level", Level: TRACE},
		{Name: "Debug Level", Level: DEBUG},
		{Name: "Info Level", Level: INFO},
		{Name: "Warn Level", Level: WARN},
		{Name: "Error Level", Level: ERROR},
		{Name: "Fatal Level", Level: FATAL},
	}

	for _, tc := range tt {
		t.Run(tc.Name, func(t *testing.T) {
			for _, lvl := range Levels() {
				log.Level = tc.Level

				t.Run(fmt.Sprintf("%s Log", lvl), func(t *testing.T) {
					log.Log(lvl, MESSAGE)
					check(t, tc.Level, lvl)
				})

				t.Run(fmt.Sprintf("%s Logf", lvl), func(t *testing.T) {
					log.Logf(lvl, "%s", MESSAGE)
					check(t, tc.Level, lvl)
				})
			}
		})
	}
}

func check(t *testing.T, current, test Level) {
	f := flush()
	m := test.Color().Format(MESSAGE)
	if current <= test && f != m {
		t.Logf("Expected `%s`, got `%s`", m, f)
		t.Fail()
	}
	if current > test && f == m {
		t.Logf("Expected no logging, got `%s`", f)
		t.Fail()
	}
}

func flush() string {
	msg := strings.TrimSpace(testBuffer.String())
	testBuffer.Reset()
	return msg
}
