package color

// Foreground text colors
const (
	FgBlack BasicAttribute = iota + 30
	FgRed
	FgGreen
	FgYellow
	FgBlue
	FgMagenta
	FgCyan
	FgWhite
)

// Foreground Hi-Intensity text colors
const (
	FgHiBlack BasicAttribute = iota + 90
	FgHiRed
	FgHiGreen
	FgHiYellow
	FgHiBlue
	FgHiMagenta
	FgHiCyan
	FgHiWhite
)

// Background text colors
const (
	BgBlack BasicAttribute = iota + 40
	BgRed
	BgGreen
	BgYellow
	BgBlue
	BgMagenta
	BgCyan
	BgWhite
)

// Background Hi-Intensity text colors
const (
	BgHiBlack BasicAttribute = iota + 100
	BgHiRed
	BgHiGreen
	BgHiYellow
	BgHiBlue
	BgHiMagenta
	BgHiCyan
	BgHiWhite
)

var attrCache = make(map[BasicAttribute]*Basic)

func attr(a BasicAttribute) *Basic {
	if c, ok := attrCache[a]; ok {
		return c
	}
	c := New(a)
	attrCache[a] = c
	return c
}

// Format is a quick way to format a string using a single attribute
func (a BasicAttribute) Format(text string) string {
	return attr(a).Format(text)
}

// Format is a quick way to format a formatted string using a single attribute
func (a BasicAttribute) Formatf(text string, v ...interface{}) string {
	return attr(a).Formatf(text, v...)
}
