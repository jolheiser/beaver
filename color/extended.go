package color

import (
	"fmt"
)

// Interface guard
var _ Color = &Extended{}

const (
	extendedFG = "38;5;"
	extendedBG = "48;5;"
)

// Extended defines a custom color object which is defined by SGR parameters.
type Extended struct {
	FG ExtendedAttribute
	BG ExtendedAttribute
}

// NewExtended returns a new Color with an ExtendedAttribute FG and BG
func NewExtended(fg, bg ExtendedAttribute) *Extended {
	return &Extended{
		FG: fg,
		BG: bg,
	}
}

// String returns a string representation of the sum of a Color's Attributes
func (e *Extended) String() string {
	return fmt.Sprintf("%d", e.FG+e.BG)
}

func (e *Extended) wrap(s string) string {
	return e.format() + s + e.unformat()
}

func (e *Extended) format() string {
	return fmt.Sprintf("%s[%s%dm%s[%s%dm", escape, extendedFG, e.FG, escape, extendedBG, e.BG)
}

func (e *Extended) unformat() string {
	return fmt.Sprintf("%s[%dm", escape, Reset)
}

// Format returns a string wrapped in the extended color
func (e *Extended) Format(text string) string {
	return e.wrap(text)
}

// Formatf returns a formatted string wrapped in the extended color
func (e *Extended) Formatf(text string, v ...interface{}) string {
	return e.wrap(fmt.Sprintf(text, v...))
}
