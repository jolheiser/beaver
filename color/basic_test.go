package color

import (
	"testing"
)

var color = New()

func TestColor(t *testing.T) {
	tt := []struct {
		Name     string
		SetAttrs []BasicAttribute
		AddAttrs []BasicAttribute
		Size     int
	}{
		{"Init", nil, nil, 0},
		{"Set 2", []BasicAttribute{Bold, FgBlack}, nil, 2},
		{"Add 2", nil, []BasicAttribute{Italic, BgWhite}, 4},
		{"Set 3", []BasicAttribute{Bold, FgBlack, BgWhite}, nil, 3},
		{"Add 3", nil, []BasicAttribute{Italic, FgWhite, BgBlack}, 6},
		{"Add Same", nil, []BasicAttribute{Italic, FgWhite, BgBlack}, 6},
		{"Add 2 Same", nil, []BasicAttribute{Italic, FgWhite, BgGreen}, 7},
	}

	for _, tc := range tt {
		t.Run(tc.Name, func(t *testing.T) {
			if tc.SetAttrs != nil {
				color.Attrs = tc.SetAttrs
			}
			if tc.AddAttrs != nil {
				color.Add(tc.AddAttrs...)
			}
			if len(color.Attrs) != tc.Size {
				t.Logf("color has `%d` attributes, but should have `%d`", len(color.Attrs), tc.Size)
			}
		})
	}
}
