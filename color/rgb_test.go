package color

import "testing"

func TestParseHex(t *testing.T) {
	tt := []struct {
		Name  string
		Hex   string
		RGB   *RGB
		Error bool
	}{
		{Name: "WhiteShort", Hex: "FFF", RGB: &RGB{255, 255, 255}, Error: false},
		{Name: "WhiteLong", Hex: "FFFFFF", RGB: &RGB{255, 255, 255}, Error: false},
		{Name: "WhiteInvalid", Hex: "F", RGB: nil, Error: true},
		{Name: "InvalidHex", Hex: "ZZZ", RGB: nil, Error: true},
	}

	for _, tc := range tt {
		t.Run(tc.Name, func(t *testing.T) {
			rgb, err := ParseHex(tc.Hex)
			if err != nil {
				if tc.Error {
					return // Pass
				}
				t.Log(err)
				t.Fail()
			}

			if rgb.Red != tc.RGB.Red || rgb.Green != tc.RGB.Green || rgb.Blue != tc.RGB.Blue {
				t.Logf("hex was parsed incorrectly: parsed %#v vs expected %#v", rgb, tc.RGB)
				t.Fail()
			}
		})
	}
}
