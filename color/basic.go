package color

import (
	"fmt"
	"strconv"
	"strings"
)

// Interface guard
var _ Color = &Basic{}

// Basic defines a custom color object which is defined by SGR parameters.
type Basic struct {
	Attrs []BasicAttribute
}

// BasicAttribute defines a single SGR Code
type BasicAttribute int

// Logger attributes
const (
	Reset BasicAttribute = iota
	Bold
	Faint
	Italic
	Underline
	BlinkSlow
	BlinkRapid
	ReverseVideo
	Concealed
	CrossedOut
)

// New returns a new Color with attrs Attributes
func New(attrs ...BasicAttribute) *Basic {
	return &Basic{Attrs: attrs}
}

// Add adds new Attributes to a Color
func (b *Basic) Add(attrs ...BasicAttribute) {
	for _, attr := range attrs {
		has := false
		for _, att := range b.Attrs {
			if attr == att {
				has = true
				break
			}
		}
		if !has {
			b.Attrs = append(b.Attrs, attr)
		}
	}
}

// String returns a string representation of the sum of a Color's Attributes
func (b *Basic) String() string {
	attrs := 0
	for _, attr := range b.Attrs {
		attrs += int(attr)
	}
	return fmt.Sprintf("%d", attrs)
}

func (b *Basic) sequence() string {
	format := make([]string, len(b.Attrs))
	for i, v := range b.Attrs {
		format[i] = strconv.Itoa(int(v))
	}

	return strings.Join(format, ";")
}

func (b *Basic) wrap(s string) string {
	return b.format() + s + b.unformat()
}

func (b *Basic) format() string {
	return fmt.Sprintf("%s[%sm", escape, b.sequence())
}

func (b *Basic) unformat() string {
	return fmt.Sprintf("%s[%dm", escape, Reset)
}

// Format returns a string wrapped in the basic color
func (b *Basic) Format(text string) string {
	if len(b.Attrs) > 0 {
		return b.wrap(text)
	}
	return text
}

// Formatf returns a formatted string wrapped in the basic color
func (b *Basic) Formatf(format string, v ...interface{}) string {
	return b.Format(fmt.Sprintf(format, v...))
}
