package color

import (
	"strings"
)

const escape = "\x1b"

type Color interface {
	Format(text string) string
	Formatf(text string, v ...interface{}) string
}

// ParseLevel parses a string and returns a Beaver Level's Color, defaulting to Info
func ParseLevel(level string) Color {
	switch strings.ToUpper(level) {
	case "T", "TRACE":
		return Trace
	case "D", "DEBUG":
		return Debug
	case "I", "INFO":
		return Info
	case "W", "WARN":
		return Warn
	case "E", "ERROR":
		return Error
	case "F", "FATAL":
		return Fatal
	}
	return Info
}

var (
	Trace   = New(Bold, FgCyan)
	Debug   = New(Bold, FgBlue)
	Info    = New(Bold, FgGreen)
	Warn    = New(Bold, FgYellow)
	Error   = New(Bold, FgRed)
	Fatal   = New(Bold, BgRed)
	Default = New()
	Time    = Default
	Stack   = Default
)
