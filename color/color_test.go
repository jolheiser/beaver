package color

import (
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	os.Exit(m.Run())
}

func TestParseLevel(t *testing.T) {
	tt := []struct {
		Parse    string
		Expected Color
	}{
		{"T", Trace},
		{"Trace", Trace},
		{"D", Debug},
		{"Debug", Debug},
		{"I", Info},
		{"Info", Info},
		{"W", Warn},
		{"Warn", Warn},
		{"E", Error},
		{"Error", Error},
		{"F", Fatal},
		{"Fatal", Fatal},
		{"Unknown", Info},
		{"N/A", Info},
		{"1234", Info},
		{"A Space", Info},
	}

	for _, tc := range tt {
		t.Run(tc.Parse, func(t *testing.T) {
			level := ParseLevel(tc.Parse)
			if level != tc.Expected {
				t.Logf("Expected `%s`, got `%s`", tc.Expected, level)
				t.Fail()
			}
		})
	}
}
