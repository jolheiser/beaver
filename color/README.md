# Color

Beaver comes with the `color` sub-package that can be used even without Beaver calls.

`Color` is an interface that simply needs `Format(text string) string` to fulfill it.  
Any logger in beaver can be set to a `Color`.

## Formatting a string with a single attribute
```go
text := color.FgRed.Format("red")
```

## Formatting a string with a full color
```go
text := color.New(color.BgGreen, color.FgRed, color.Bold).Format("green background, red text, and bold")
```

## Extended colors

Extended colors are 256-color extensions. They can be referred to by name, available in [extended_colors.go](extended_colors.go).

## True colors

True colors are full RBG colors, they can be either created with RGB values or parsed from hex.