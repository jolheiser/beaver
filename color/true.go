package color

import "fmt"

// Interface guard
var _ Color = &True{}

const (
	trueFG = "38;2;"
	trueBG = "48;2;"
)

type True struct {
	FG *RGB
	BG *RGB
}

// NewTrue returns a new Color with RGB FG and BG
func NewTrue(fg, bg *RGB) *True {
	return &True{
		FG: fg,
		BG: bg,
	}
}

// String returns a string representation of the sum of a Color's Attributes
func (t *True) String() string {
	return fmt.Sprintf("%d", t.FG.Red+t.FG.Green+t.FG.Blue+t.BG.Red+t.BG.Green+t.BG.Blue)
}

func (t *True) wrap(s string) string {
	return t.format() + s + t.unformat()
}

func (t *True) format() string {
	return fmt.Sprintf("%s[%s%sm%s[%s%sm", escape, trueFG, t.FG.format(), escape, trueBG, t.BG.format())
}

func (t *True) unformat() string {
	return fmt.Sprintf("%s[%dm", escape, Reset)
}

// Format returns a string wrapped in the true color
func (t *True) Format(text string) string {
	return t.wrap(text)
}

// Formatf returns a formatted string wrapped in the true color
func (t *True) Formatf(text string, v ...interface{}) string {
	return t.wrap(fmt.Sprintf(text, v...))
}
