# beaver

Make short work of your logs

## Loggers
Beaver comes ready to log to `stdout` via shorthand `beaver.Info` etc. usage.  
However, Beaver can also create new standalone loggers with `beaver.New`.  
Beaver loggers can write to anything implementing the `io.Writer` interface.

## Options
|    Option    |   Type  |                          Effect                         |
|:------------:|:-------:|:-------------------------------------------------------:|
|  TimePrefix  | boolean | Prepends the date/time                                  |
|  StackPrefix | boolean | Prepends the calling file/line                          |
|  StackLimit  | integer | Amount of calling file to show, defaults to entire path |
| LevelPrefix  | boolean | Prepends a logging level prefix, e.g. [T] for Trace     |
|  LevelColor  | boolean | Colors the LevelPrefix if enabled                       |
| MessageColor | boolean | Colors the message itself                               |

The default Console configuration is below  
Colored messages convey the logging level while reducing the amount of space used for CLI applications

|    Option    | Value |
|:------------:|:-----:|
|  TimePrefix  | false |
|  StackPrefix | false |
|  StackLimit  |   0   |
| LevelPrefix  | false |
|  LevelColor  | false |
| MessageColor | true  |


## Colors
Beaver allows you to customize the colors of various parts of the message

| `color.` | Default Format |
|:--------:|----------------|
| Trace    | Bold, FgCyan   |
| Debug    | Bold, FgBlue   |
| Info     | Bold, FgGreen  |
| Warn     | Bold, FgYellow |
| Error    | Bold, FgRed    |
| Fatal    | Bold, BgRed    |
| Default  | None           |
| Time     | `Default`      |
| Stack    | `Default`      |

```go
// Set Trace logging to Magenta instead of Bold-Cyan
color.Trace = color.New(color.FgMagenta)
```

[More info for the `color` package](color/README.md)

## LICENSE

[MIT](LICENSE)