package beaver

import (
	"fmt"
	"testing"
)

func TestConsole(t *testing.T) {
	Console.Writer = testBuffer
	tt := []struct {
		Name  string
		Level Level
	}{
		{Name: "Trace Level", Level: TRACE},
		{Name: "Debug Level", Level: DEBUG},
		{Name: "Info Level", Level: INFO},
		{Name: "Warn Level", Level: WARN},
		{Name: "Error Level", Level: ERROR},
		{Name: "Fatal Level", Level: FATAL},
	}

	for _, tc := range tt {
		t.Run(tc.Name, func(t *testing.T) {
			for _, lvl := range Levels() {
				Console.Level = tc.Level

				t.Run(fmt.Sprintf("%s Log", lvl), func(t *testing.T) {
					Log(lvl, MESSAGE)
					check(t, tc.Level, lvl)
				})

				t.Run(fmt.Sprintf("%s Logf", lvl), func(t *testing.T) {
					Logf(lvl, "%s", MESSAGE)
					check(t, tc.Level, lvl)
				})
			}
		})
	}
}
