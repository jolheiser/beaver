package beaver

import (
	"os"
)

var (
	Console = &Logger{
		Writer: os.Stdout,
		Level:  INFO,
		Format: FormatOptions{
			MessageColor: true,
		},
		skip: 4,
	}
)

// Low-Level Log
func Log(level Level, v ...interface{}) {
	Console.Log(level, v...)
}

// Low-Level formatted Log
func Logf(level Level, format string, v ...interface{}) {
	Console.Logf(level, format, v...)
}

// Trace logs at TRACE Level
func Trace(v ...interface{}) {
	Console.Trace(v...)
}

// Tracef formats logs at TRACE Level
func Tracef(format string, v ...interface{}) {
	Console.Tracef(format, v...)
}

// Debug logs at DEBUG Level
func Debug(v ...interface{}) {
	Console.Debug(v...)
}

// Debugf formats logs at DEBUG Level
func Debugf(format string, v ...interface{}) {
	Console.Debugf(format, v...)
}

// Info logs at INFO Level
func Info(v ...interface{}) {
	Console.Info(v...)
}

// Infof formats logs at INFO Level
func Infof(format string, v ...interface{}) {
	Console.Infof(format, v...)
}

// Warn logs at WARN Level
func Warn(v ...interface{}) {
	Console.Warn(v...)
}

// Warnf formats logs at WARN Level
func Warnf(format string, v ...interface{}) {
	Console.Warnf(format, v...)
}

// Error logs at ERROR Level
func Error(v ...interface{}) {
	Console.Error(v...)
}

// Errorf formats logs at ERROR Level
func Errorf(format string, v ...interface{}) {
	Console.Errorf(format, v...)
}

// Fatal logs at FATAL Level
func Fatal(v ...interface{}) {
	Console.Fatal(v...)
	os.Exit(1)
}

// Fatalf formats logs at FATAL Level
func Fatalf(format string, v ...interface{}) {
	Console.Fatalf(format, v...)
	os.Exit(1)
}
