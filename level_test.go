package beaver

import "testing"

func TestParseLevel(t *testing.T) {
	tt := []struct {
		Parse    string
		Expected Level
	}{
		{"T", TRACE},
		{"Trace", TRACE},
		{"D", DEBUG},
		{"Debug", DEBUG},
		{"I", INFO},
		{"Info", INFO},
		{"W", WARN},
		{"Warn", WARN},
		{"E", ERROR},
		{"Error", ERROR},
		{"F", FATAL},
		{"Fatal", FATAL},
		{"Unknown", INFO},
		{"N/A", INFO},
		{"1234", INFO},
		{"A Space", INFO},
	}

	for _, tc := range tt {
		t.Run(tc.Parse, func(t *testing.T) {
			level := ParseLevel(tc.Parse)
			if level != tc.Expected {
				t.Logf("Expected `%s`, got `%s`", tc.Expected, level)
				t.Fail()
			}
		})
	}
}
